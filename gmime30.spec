Name:		gmime30
Version:	3.2.15
Release:	1
Summary:	Library for parsing and creating messages using MIME

License:	LGPL-2.1-or-later
URL:		https://github.com/jstedfast/gmime
Source0:	https://github.com/jstedfast/gmime/releases/download/%{version}/gmime-%{version}.tar.xz
BuildRequires: pkgconfig(gio-2.0)
BuildRequires: pkgconfig(glib-2.0) >= 2.68
BuildRequires: pkgconfig(gobject-2.0)
BuildRequires: pkgconfig(gobject-introspection-1.0) >= 1.30.0
BuildRequires: pkgconfig(gpg-error)
BuildRequires: pkgconfig(gpgme) >= 1.6.0
BuildRequires: pkgconfig(gthread-2.0)
BuildRequires: pkgconfig(libidn2) >= 2.0.0
BuildRequires: pkgconfig(zlib)
BuildRequires: gtk-doc >= 1.8
BuildRequires: vala

%description
GMime is a set of utilities for parsing and creating messages using
the Multipurpose Internet Mail Extension (MIME).

%package	devel
Summary:	headers for gmime
Requires:	%{name} = %{version}-%{release}

%description	devel
headers for gmime30.

%package_help

%prep
%autosetup -n gmime-%{version} -p1

%build
%configure --disable-static
%make_build

%install
%make_install
%delete_la

%check
%make_build check

%files
%doc AUTHORS README README.md
%license COPYING
%{_libdir}/*.so.*
%{_libdir}/girepository-1.0/*.typelib

%files devel
%{_includedir}/gmime-3.0
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/gir-1.0/*.gir
%{_datadir}/vala/vapi/*

%files help
%{_datadir}/gtk-doc/html/gmime*

%changelog
* Sat Jan 11 2025 Funda Wang <fundawang@yeah.net> - 3.2.15-1
- update to 3.2.15

* Thu Jul 22 2021 panxiaohe<panxiaohe@huawei.com> - 3.2.7-2
- remove unnecessary BuildRequires: gdb

* Wed Jan 20 2021 wangchen<wangchen137@huawei.com> - 3.2.7-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to 3.2.7

* Thu Mar 19 2020 chengquan<chengquan3@huawei.com> - 3.2.0-4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add necessary BuildRequires

* Tue Sep 24 2019 shenyangyang<shenyangyang4@huawei.com> - 3.2.0-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:revise help package

* Thu Aug 15 2019 openEuler Builteam <buildteam@openeuler.org> - 3.2.0-2
- Package init
